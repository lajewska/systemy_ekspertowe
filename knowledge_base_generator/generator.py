import random


def get_country():
    countries = ['greece', 'spain', 'turkey', ' italy', ' france', ' austria', ' china', ' germany', ' thailand', ' united_kingdom', ' germany', ' portugal', ' vietnam', ' mexico', ' argentina', ' egypt', ' morocco', ' united_states', ' canada', ' brasil']
    return random.choice(countries)


def get_season():
    seasons = ['spring', 'summer', 'autumn', 'winter']
    return random.choice(seasons)


def get_transport():
    means_of_transport = ['plain', 'bus', 'train', 'on_your_own']
    return random.choice(means_of_transport)


def get_trip_type():
    trip_types = ['round_trip', 'skiing', 'cruise', 'leisure']
    return random.choice(trip_types)


def get_alimentation():
    alimentation_types = ['all_inclusive', 'full', 'two_meals', 'breakfast', 'without']
    return random.choice(alimentation_types)


def get_standard():
    standards = ['three_stars', 'four_stars', 'five_stars']
    return random.choice(standards)


def get_is_suitable_for_children():
    suitable_for_children = ['yes', 'no']
    return random.choice(suitable_for_children)


def get_prize():
    return random.randint(0, 50000)


def get_duration():
    return random.randint(2, 30)


def get_scale_measured_values():
    return random.randint(1, 10)


feature_schema = [ ('country', get_country),
                   ('season', get_season),
                   ('transport', get_transport),
                   ('trip_type', get_trip_type),
                   ('alimentation', get_alimentation),
                   ('standard', get_standard),
                   ('suitable_for_children', get_is_suitable_for_children),
                   ('prize', get_prize),
                   ('duration', get_duration),
                   ('comfort', get_scale_measured_values),
                   ('exclusiveness', get_scale_measured_values),
                   ('exoticism', get_scale_measured_values),
                   ('active_time', get_scale_measured_values),
                   ('sightseeing', get_scale_measured_values),
                   ('free_time', get_scale_measured_values),
                   ('good_food', get_scale_measured_values)]


def read_file_with_trips():
    file = open("trips.txt", 'r')
    trips = []
    for trip in file.readlines():
        trips.append(trip.rstrip())

    return trips


def generate_file_with_features():
    file = open("features.txt", 'w')

    trips = read_file_with_trips()
    num_of_trips = len(trips)

    for index, trip in enumerate(trips):
        file.write('trip_info(' + str(index + 1) + ', \'' + trip + '\').\n')

    file.write('\n')

    for feature, function in feature_schema:
        for index in range(num_of_trips):
            file.write('trip_feature(' + str(index + 1) + ', ' + feature + ', ' + str(function()) + ').\n')

        file.write('\n')

    file.close()


if __name__ == '__main__':
    generate_file_with_features()



