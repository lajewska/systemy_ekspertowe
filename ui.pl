:- consult('knowledge_base.pl').
:- consult('rules.pl').
:- consult('reason_trip2.pl').
:- use_module(library(random)).
:- consult('tty.pl').

main_menu :-
  repeat,
  format('~n## Travel agency ##'),
  d_menu('   Choose an option:',
  [ print_one: 'Single trip information',
    print_random: 'Random trip information',
    print_all: 'Basic information on all trips',
    start_reasoning: 'Find most suitable trip',
    quit: 'Quit program'], Opt),
  writeln(''),
  do_main_opt(Opt),fail.

do_main_opt(print_one):-print_one.
do_main_opt(print_random):-print_random.
do_main_opt(print_all):-print_all.
do_main_opt(start_reasoning):-start_reasoning.
do_main_opt(quit):-quit.

d_menu(Title,List,Choice):-
    format('~n~n'),writeln(Title),
    tty:build_menu(List),
    tty:get_answer(List, Choice).



list_menu_options :-
  menu_option(Action, Description),
  format('   - ~p. ~s~n', [Action, Description]),
  fail.

list_menu_options :- !.


print_one :-
  format('# Select a trip ID: '), read(Id), nl,
  (print_trip_info(Id);get_single_char(_)),!.



print_random :-
  aggregate_all(count, trip_info(_X, _Y), Count),
  random_between(1, Count, Rd),
  print_trip_info(Rd).



stars(three_stars, '***').
stars(four_stars, '****').
stars(five_stars, '*****').

print_all :-
  trip_info(Id, TripName),
  trip_feature(Id, country, CountryValue),
  trip_feature(Id, season, SeasonValue),
  trip_feature(Id, standard, Std),
  trip_feature(Id, prize, PrizeValue),
  stars(Std, StandardValue),
  format('Trip(~w): ~w, ~w, ~w, €~2f (~w)~n', [Id, TripName, CountryValue, SeasonValue, PrizeValue rdiv 10, StandardValue]),get_single_char(_),
  fail.

print_all :- !.

print_trip_info(Id) :-
  trip_info(Id, Name), format('Trip id: ~w, name: ~w~n', [Id, Name]),
  trip_feature(Id, FeatureType, Value), feature(FeatureType, FeatureName), format('~w: ~w~n', [FeatureName, Value]),
  fail.

print_trip_info(_Id) :- !.

start_reasoning:-
    clear_reasoning,
    reason.

reason :-
    repeat,
    reason_trips,
%    print_result,
    printp,
%    print_best(30),
    reason_menu(Opt),
    ((Opt==end,!);
    fail).
          


% Print result
print_result :-
    format('~nBest matching trips:~n'),
%    findall(N-X, result(X,N), Pairs0),keysort(Pairs0,Pairs),
%    reverse(Pairs,Reversed,[]),
    results(BestTr),
    print_results(BestTr).

print_results([]).
print_results([X-Y|B]) :-
    result(Y, X),
    trip_info(Y, Disease),
    print_trip(Y, Disease, X),
    print_results(B).
print_results :- !.

printp:-
   format('~nBest matching trips:~n'),
   results(BestTr),
   convlist([[Id,Score],Id]>>(Score>0,trip_info(Id,Name),
      print_trip(Id,Name,Score)),BestTr,_).

%print_result :-
%    print_results.
%
%print_results :-
%    result(Id, Score),
%    trip_info(Id, Name),
%    print_trip(Id, Name, Score),
%    fail.
%print_results :- !.

print_best(N):-
   format('~nBest matching trips:~n'),
   results(BestTr),
   findnsols(N,Tr,member(Tr,BestTr),Best10),!,
   convlist([[Id,Score],Id]>>(Score>0.3,trip_info(Id,Name),
      print_trip(Id,Name,Score)),Best10,_).

reason_menu(Opt):-
   d_menu('Next choice',[narrow:'Narrow selection',alternate:'Add value to feature'
   ,del:'Delete selected feature',show_sel:'Show current features',
   trip_info:'Trip detailed information',
   end:'End selection'],Opt),!,
   writeln(''),
   do_opt(Opt),!.
   
reverse([],Z,Z).

reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).

do_opt(end):- main_menu.
do_opt(narrow):-next_feature.
do_opt(alternate):-opt_feature.
do_opt(del):-del_feature.
do_opt(trip_info):-print_one.
do_opt(show_sel):-print_sel_opts.


print_sel_opts:-
   answered(Features),
   findall([Name,Answers],(feature(Feat,Name),member([Feat,Answers],Features)),SelL),
   format('~nCurrently selected:~n'),
   maplist(writeln,SelL),
   get_single_char(_),!.

print_trip(Id, Name, Score) :-
    format('|~p~t~5||~s~t~30||~2f~t~40||~n', 
        [Id, Name, Score]).

quit :-
  halt(0).

:- main_menu.
