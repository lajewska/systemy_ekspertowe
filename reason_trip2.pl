:-[knowledge_base].
:-[rules].
:-use_module(library(clpfd)).

:- dynamic(progress/2).
:- dynamic(result/2).


      

opt_feature:-
     answered(Features),
     findall(Feat:Name,(
       feature(Feat,Name),
       member([Feat,_],Features)
       ),FL),

     d_menu('Which feature?',FL,PickedF),
     member([PickedF,AnswerL],Features),
     writeln(''),
     write('Current selection:'),writeln(AnswerL),
     askfull(PickedF,AltAnswer),
     (AltAnswer='';(
     delete(Features,[PickedF,_],FeaturesTemp),
     retractall(answered(_)),
     assertz(answered([[PickedF,[AltAnswer|AnswerL]]|FeaturesTemp])))),!.

next_feature():-
    answered(Features),
    findall(Feat:Name,(
       feature(Feat,Name),
       not(member([Feat,_],Features))
       ),FL),
    menu('Name your wish?',FL,PickedF),
    writeln(''),
    ask(PickedF,Answer),
    (Answer='';
    (retractall(answered(_)),
    assertz(answered([[PickedF,[Answer]]|Features])))),!.

del_feature:-
     answered(Features),
     findall(Feat:Name,(
       feature(Feat,Name),
       member([Feat,_],Features)
       ),FL),

     d_menu('Which feature to remove from selection?',FL,PickedF),
     delete(Features,[PickedF,_],FeaturesTemp),
     retractall(answered(_)),
     assertz(answered(FeaturesTemp)),!.


reason_trips:-
  answered([]),
  findall([Id,0],(trip_info(Id,_)),Results),
  retractall(results(_)),
    assertz(results(Results)),!
  .
    

reason_trips:-
    answered(Features),
    
    findall([Id,Score],(trip_info(Id,_),reason_trip(Id,Score)),ScoreResult),
    sort(2,@>=,ScoreResult,SortedResult),
    retractall(results(_)),
    assertz(results(SortedResult)).


reason_trip(Id,Score) :-
    findall({Feature, Value}, (feature(Feature,_),evaluate_trip_feature(Id, Feature, Value)), FVs),
    calculate_score(FVs, 1, Score)
    .

get_score(OldScore, NewScore, Score) :-
%T-Norm for fuzzy "and" operator
    Score is OldScore*NewScore.
    % Score is NewScore.

calculate_score([], Score, Score).

%Sharpening is about to be done here
calculate_score([{Feature, Value} | _], OldScore, 0) :-
    ((get_answer(Feature, no_selection),NewScore=1);
     (get_answer(Feature,AnswerL),
    convlist([A,Sc]>>score(Feature, A, Value, Sc),AnswerL,ScL),
    max_list(ScL,NewScore))),
    get_score(OldScore, NewScore, TempScore),
    TempScore =< 0.5,!.

%calculate_score([{Feature, Value} | _], OldScore, 0) :-
%    ((get_answer(Feature, no_selection),NewScore=1,!);
%     (findall(Sc,(get_answer(Feature,Answer),
%    score(Feature, Answer, Value, Sc)),ScL),
%%S-norm for fuzzy "or" operator as max
%    max_list(ScL,NewScore))),
%    get_score(OldScore, NewScore, TempScore),
%    TempScore < 0.5,!.

%calculate_score([{Feature, Value} | _], OldScore, 0) :-
%    get_answer(Feature, Answer),
%    score(Feature, Answer, Value, NewScore),
%    get_score(OldScore, NewScore, TempScore),
%    TempScore =< 0.5, !.
%calculate_score([{Feature, Value} | Tail], OldScore, Score) :-
%    get_answer(Feature, Answer),
%    score(Feature, Answer, Value, NewScore),
%    % TempScore is min(OldScore, NewScore),
%    get_score(OldScore, NewScore, TempScore),
%    calculate_score(Tail, TempScore, Score).


calculate_score([{Feature, Value} | Tail], OldScore, Score) :-
    ((get_answer(Feature, no_selection),NewScore=1,!);
     (findall(Sc,(get_answer(Feature,Answer),
    score(Feature, Answer, Value, Sc)),ScL),
%S-norm for fuzzy "or" operator as max
    max_list(ScL,NewScore))),
    get_score(OldScore, NewScore, TempScore),
    calculate_score(Tail, TempScore, Score).

get_answer(Feature, Answer) :-
    answered(Features),
    ((member([Feature,AnswerL],Features),member(Answer,AnswerL));
    (not(member([Feature,_],Features)),Answer=no_selection,!)).



ask(Feature, Answer) :-
    feature_type(Feature,FType,Range1),
    question(Feature, Question),writeln(''),
    suggestion(FType,Feature,Range1,Range2),
    write('Suggested values: '),
    writeln(Range2),
    write(Question), 
    writeln(''), read(Answer).

askfull(Feature, Answer) :-
    feature_type(Feature,FType,Range1),
    question(Feature, Question),writeln(''),

    write('Suggested values: '),
    writeln(Range1),
    write(Question), 
    writeln(''), read(Answer).

suggestion(number,Feature,Range1,Range2):-
    results(Results),
    findall(Fval,(trip_info(Id,_),member([Id,Score],Results),
              Score>=0.5,evaluate_trip_feature(Id,Feature,Fval)),Fvallist),
    length(Fvallist,0),
    Range2=Range1.

suggestion(number,Feature,Range1,Range2):-
    results(Results),
    findall(Fval,(trip_info(Id,_),member([Id,Score],Results),
              Score>=0.5,evaluate_trip_feature(Id,Feature,Fval)),Fvallist),
    not(length(Fvallist,0)),
    min_list(Fvallist,X1),
    max_list(Fvallist,X2),Range2=[X1,X2].

suggestion(atom,Feature,Range1,Range2):-
    results(Results),
    findall(Fval,(trip_info(Id,_),member([Id,Score],Results),
              Score>=0.5,evaluate_trip_feature(Id,Feature,Fval)),Fvallist),
    length(Fvallist,0),
    Range2=Range1.

suggestion(atom,Feature,Range1,Range2):-
    results(Results),
    findall(Fval,(trip_info(Id,_),member([Id,Score],Results),
              Score>=0.5,evaluate_trip_feature(Id,Feature,Fval)),Fvallist),
    not(length(Fvallist,0)),
    list_to_set(Fvallist,Range2).





clear_reasoning :-
    findall({Id,0},trip_info(Id,_),Results),
    assertz(results(Results)),
    retractall(answered(_)),
    assertz(answered([])),
    retractall(result(_, _)), !.
clear_reasoning.
