% features (categories)

feature(country, 'Country').
feature(season, 'Season').
feature(transport, 'Transport').
feature(trip_type, 'Trip_type').
feature(alimentation, 'Alimentation').
feature(standard, 'Standard').
feature(suitable_for_children, 'Suitable_for_children').

% features (scale-measured)

feature(prize, 'Prize').
feature(value4money, 'Value_for_money').
feature(duration, 'Duration').
feature(comfort, 'Comfort').
feature(exclusiveness, 'Exclusiveness').
feature(exoticism, 'Exoticism').
feature(active_time, 'Active_time').
feature(sightseeing, 'Sightseeing').
feature(free_time, 'Free_time').
feature(good_food, 'Good_food').


% possible features' values

feature_type(country, atom, [greece, spain, turkey, italy, france, austria, china, thailand, united_kingdom, germany, portugal, vietnam, mexico, argentina, egypt, morocco, united_states, canada, brasil]).
feature_type(season, atom, [spring, summer, autumn, winter]).
feature_type(transport, atom, [plane, bus, train, on_your_own]).
feature_type(trip_type, atom, [round_trip, skiing, cruise, leisure]).
feature_type(alimentation, atom, [all_inclusive, full, two_meals, breakfast, without]).
feature_type(standard, atom, [three_stars, four_stars, five_stars]).
feature_type(suitable_for_children, atom, [yes, no]).
feature_type(prize, number, [0, 50000]).
feature_type(value4money,number,[1,10]).
feature_type(duration, number,  [2, 30]).
feature_type(comfort, number, [1, 10]).
feature_type(exclusiveness, number, [1, 10]).
feature_type(exoticism, number, [1, 10]).
feature_type(active_time, number, [1, 10]).
feature_type(sightseeing, number, [1, 10]).
feature_type(free_time, number, [1, 10]).
feature_type(good_food, number, [1, 10]).


% questions

question(country, 'What country do you want to go to?').
question(season, 'In which part of the year would you like to go?').
question(transport, 'Which mean of transport do you prefer?').
question(trip_type, 'What type of trip are you interested in?').
question(alimentation, 'What type of alimentation do you prefer?').
question(standard, 'What standard of the hotel do you require?').
question(suitable_for_children, 'Should the trip be suitable for children?').
question(prize, 'What is the approximate prize of the trip you are willing to pay?').
question(value4money,'What is value (1 to 10) for money expectation?').
question(duration, 'What is the desired duration (min_days, max_days) of the trip (in days)?').
question(comfort, 'How important (from 1 to 10) is comfort for you?').
question(exclusiveness, 'How important (from 1 to 10) is exclusiveness for yuo?').
question(exoticism, 'How important (from 1 to 10) is exoticism for you?').
question(active_time, 'How important (from 1 to 10) is the posibility to spend your time actively?').
question(sightseeing, 'How important (from 1 to 10) is sightseeing for you?').
question(free_time, 'How important (from 1 to 10) is free time for you?').
question(good_food, 'How important (from 1 to 10) is the availability of good and/or local food for you?').

%%% reasoning about features
%%% with defuzzification

%when feature is direct attribute of trip
evaluate_trip_feature(Id,Feature,Trip_feature):-trip_feature(Id,Feature,Trip_feature),!.

%when feature is evaluated from other features
evaluate_trip_feature(Id,Feature,Trip_feature):-feature_evaluation(Id,Feature,Trip_feature),!.

feature_evaluation(Id,Feature,EvaluatedValue):-
           findall([RuleValue,Value],(
                feature_implication(FL,VL,Feature,Value,Norm),
                implication_calculate(Id,FL,VL,RuleValue,Norm)
                
           ),RVV),not(length(RVV,0)),
           defuzzify(RVV,EvaluatedValue).
          


 
defuzzify(RVV,EvaluatedValue):-sum_xiuxi(RVV,S1),sum_uxi(RVV,S2),((S2>0,EvaluatedValue is S1/S2,!);(EvaluatedValue is 0,!)).

sum_xiuxi([[Uxi,(Xi1,Xi2)]|[]],S1):-S1 is (Xi1+Xi2)*Uxi/2,!.
sum_xiuxi([[Uxi,Xi]|[]],S1):-S1 is Xi*Uxi,!.
sum_xiuxi([[Uxi,(Xi1,Xi2)]|RVV],S1):-sum_xiuxi(RVV,S2),S1 is (S2 + (Xi1+Xi2)*Uxi/2),!.
sum_xiuxi([[Uxi,Xi]|RVV],S1):-sum_xiuxi(RVV,S2),S1 is S2 + Xi*Uxi,!.

sum_uxi(RVV,S1):-transpose(RVV,[UxiL,_]),sumlist(UxiL,S1).


implication_calculate(Id,[F|[]],[V|[]],Value,Norm):-evaluate_trip_feature(Id,F,TFV),
       score(F,V,TFV,Value),!.

implication_calculate(Id,[F|FL],[V|VL],Value,Norm):-
       evaluate_trip_feature(Id,F,TFV),
       score(F,V,TFV,FScore1),implication_calculate(Id,FL,VL,FScore2,Norm),
       norm(Norm,FScore1,FScore2,Value),!.


norm(tnorm,P,Q,S):-S is P*Q.
norm(snorm,P,Q,S):-S is max(P,Q).



% scoring functions
score(trapez, (Min_ans, Max_ans), Value, Score) :-
    (Min_ans =< Value), (Max_ans >=Value)
    -> Score is 1
    ; Score is max(0, min(
            (Value-(Min_ans-3)) / 3,
            ((Max_ans+3)-Value) / 3) ).
score(exp_score, Client_answer, Trip_feature, Score,Sigma) :-
    Score is max(0, exp(-(((Client_answer - Trip_feature) / Sigma)^2))),!.

score(sharp, Client_answer, Trip_feature, 1) :- Client_answer = Trip_feature.

score(sharp, [], _, 0).
score(sharp, [H|T], Trip_feature, Score) :-
    H = Trip_feature
    -> Score is 1
    ; score(country, T, Trip_feature, Score).
score(sharp,Client_answer,Trip_feature,0):-Client_answer \= Trip_feature.

%fuzzy function assignment to features

score(country, Client_answer, Trip_feature, Score) :- 
       score(sharp,Client_answer, Trip_feature, Score),!.

score(season, Client_answer, Trip_feature, Score) :- score(sharp,Client_answer, Trip_feature, Score),!.

score(transport, Client_answer, Trip_feature, Score) :- score(sharp,Client_answer, Trip_feature, Score),!.

score(trip_type, Client_answer, Trip_feature, Score) :- score(sharp,Client_answer, Trip_feature, Score),!.

score(alimentation, Client_answer, Trip_feature, Score) :- score(sharp,Client_answer, Trip_feature, Score),!.

score(standard, Client_answer, Trip_feature, Score) :-
    score(sharp,Client_answer, Trip_feature, Score),!.

score(suitable_for_children, Client_answer, Trip_feature, Score) :-
    score(sharp,Client_answer, Trip_feature, Score),!.

score(prize, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(prize, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,Client_answer),!.

score(value4money, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(value4money, Client_answer, Value, Score) :-
    score(exp_score, Client_answer, Value, Score,4),!.


score(duration, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(duration, Client_answer, Value, Score) :-
    score(exp_score, Client_answer, Value, Score,4),!.


score(comfort,(Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(comfort, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3),!.
    

score(exclusiveness,(Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(exclusiveness, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3).

score(exoticism,(Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(exoticism, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3),!.

score(active_time, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(active_time, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3),!.


score(sightseeing, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(sightseeing, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3),!.

score(free_time, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(free_time, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3),!.

score(good_food, (Min_ans, Max_ans), Value, Score) :-
    score(trapez, (Min_ans, Max_ans), Value, Score),!.

score(good_food, Client_answer, Trip_feature, Score) :-
    score(exp_score, Client_answer, Trip_feature, Score,3),!.



